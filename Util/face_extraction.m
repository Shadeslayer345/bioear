clear all;close all;

% folder of source with all files in one folder
path='\\skynet\static\Processed_Images\MBGCv2NIRFACE\MBGCv2NIRFACE_FACE\';
% eye locations file
fid = fopen('\\skynet\static\Processed_Images\MBGCv2NIRFACE\eyeloc_for_mbgcv2_3979.txt','rt');
% variables
counter = 1;
names = {};
perdark = [];

% get a line to start the loop
s = fgetl(fid);
for i=1:2440
    s = fgetl(fid);
end
while (ischar(s))                   % while not end of file   
	% pick a delimiter to separate the file name from the file prefix. Period is a bad choice in case the eye center coords are decimals
    [name b] = strread(s, '%s %s', 'delimiter', '.');
    [jpg, lx, ly, rx, ry] = strread(char(b), '%s %f %f %f %f');
	%[dirname nothing] = strread(char(name), '%s %s', 'delimiter', '/');
    %name = strcat(dirname, '\', nothing);

	% choose a lossless output filetype
    FaceName = ['\\skynet\static\Processed_Images\MBGCv2NIRFACE\MBGCv2NIRFACE_ALIGNEDFACE\' char(name) '.ppm'];
	
	%if(~exist(['\\skynet\static\Processed_Images\Pinellas\Pinellas_ALIGNEDFACE\' char(dirname) '\'], 'dir'))
	%   mkdir(['\\skynet\static\Processed_Images\Pinellas\Pinellas_ALIGNEDFACE\' char(dirname) '\']);
	%end

	% find border region size
    EyeWidth = lx-rx;
    InnerEyeWidth = int32(EyeWidth * .5);
    OuterEyeWidth = int32(EyeWidth * .5);
    VerticalEyeWidth = int32(EyeWidth * .5);

    im=imread([char(path) char(name) '.' char(jpg)]);
    im2 = im;
    
	% rotation correction code
    if (ry ~= ly)
        a = [rx ry];
        b = [lx ly];
        c = [lx ry];
        diff = ((b-a)./2);
        center = diff+a;
        ba = sqrt(sum((b-a).^2));
        ca = sqrt(sum((c-a).^2));
        cb = sqrt(sum((c-b).^2));
        A = atand(cb/ca);
        if (ry > ly)
            A = -A;
        end
        T = [cosd(A) sind(A); -sind(A) cosd(A)];
        center = [size(im,2), size(im,1)] / 2;
        a2 = a-center;
        b2 = b-center;
        a3 = T*a2';
        b3 = T*b2';
        ap = a3+center';
        bp = b3+center';
        rx = int32(ap(1));
        ry = int32(ap(2));
        lx = int32(bp(1));
        ly = int32(bp(2));
        im = imrotate(im,A,'bilinear','crop');
    end

    top = ly-int32(EyeWidth * 1.0);
    bottom = ly+int32(EyeWidth * 1.5);
    left = rx-int32(EyeWidth * 0.5);
    right = lx+int32(EyeWidth * 0.5);
	% if good image then write, else put in bad list
    if (top > 0 && bottom <= size(im,1) && left > 0 && right <= size(im,2))
        Face = im(top:bottom,left:right,:);
        imwrite(Face, FaceName);
		names = [names;strcat(name,'.ppm')];
		perdark = [perdark;0];
    else
		Face = zeros(bottom-top+1,right-left+1,3,'uint8');
		offsettop = 1;
        offsetbottom = size(Face,1);
		offsetleft = 1;
        offsetright =size(Face,2); 
		if (top <= 0)
			offsettop = -top+2;
            top = 1;
		end
		if (bottom > size(im,1))
			offsetbottom = size(Face,1)-(bottom - size(im,1));
            bottom = size(im,1);
		end
		if (left <= 0)
			offsetleft = -left+2;
            left = 1;
		end
		if (right > size(im,2))
			offsetright = size(Face,2)-(right - size(im,2));
            right = size(im,2);
        end
        facetemp = im(top:bottom,left:right,:);
		Face(offsettop:offsetbottom,offsetleft:offsetright,:) = im(top:bottom,left:right,:);
		imwrite(Face, FaceName);
		names = [names;strcat(name,'.ppm')];
		perdark = [perdark;sum(sum(Face(:,:,1)==0 & Face(:,:,2)==0 & Face(:,:,3)==0))/(size(Face,1)*size(Face,2))];
    end

    % debugging
%     subplot(2,2,1), imshow(im2);
%     title('Original Image');
%     subplot(2,2,2), imshow(im);
%     title('Rotated Image');
%     subplot(2,2,4), imshow(FaceName);
%     title([num2str(lx) ' ' num2str(ly)]);
    
    
    %leftEyeRegion2 = imresize(FaceName, [100 160]);

	% type = 1 for using inside matlab environment
	% type = 2 for command line matlab
	type = 1;
	if type == 1
		str = sprintf('Finished extraction for %d images.', counter);
		lstr = length(str);
		if(counter==1)
			disp(['     ' str]);
		elseif (counter==2)
			[char(8)*ones(1,lstr+9) ,str]
		else
			[char(8)*ones(1,lstr+10) ,str]
		end
	elseif type == 2
		fprintf('Finished extraction for %d images.\r', counter);
	end
	
	
    s = fgetl(fid);
    counter=counter+1;
end;
fprintf(1, 'Faces complete. %12.0f source files\n', counter);
fclose(fid);

%disp('Bad Names');
%disp(badnames);


