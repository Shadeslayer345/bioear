%%Pinellas Demographic Figures

base_dir = '/group/bprl/datafiles1/FACE/PINELLAS EXTRACT/IMAGE_DATA_EXTRACTED_FILES_PINELLAS/';
alignedface_dir = '/group/bprl/static/Processed_Images/Pinellas/Pinellas_ALIGNEDFACE/';
peri_dir = '/group/bprl/static/Processed_Images/Pinellas/Pinellas_PERI/';

id_num = cell(1447607,1);
race = cell(1447607,1);
gender = cell(1447607,1);
age = zeros(1447607,1);
img_name = cell(1447607,1);
have_peri = zeros(1447607,1);
peri_size = zeros(1447607,1);
quality = zeros(1447607,1); 
%expression = cell(1447607,1);
%usedfor = cell(1447607,1);
%lighting = cell(1447607,1);
%lefteyex = zeros(1447607,1);
%lefteyey = zeros(1447607,1);
%righteyex = zeros(1447607,1);
%righteyey = zeros(1447607,1);


cur = 1;
for i=0:144%200
   fprintf('%d ',i);

   img_dir=sprintf('DIR_%03d/',i);
   file_name=sprintf('DEM_%03d.txt',i);
   file = [base_dir img_dir file_name];
   
   fid=fopen(file);
   %personID-3, race-4, DOB-5, ImageDate-19, gender-14
   %data=textscan(fid, '%[^,],%d,%[^,],%[^,],%[^,],%d,%d,%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%d,%[^,],%[^,\n]');
   data=textscan(fid, '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s', 'delimiter', ',', 'ReturnOnError', 0, 'EmptyValue', -Inf);
   fclose(fid);
      
   %age calculations
   dob=datestr(data{5});
   doi=datestr(data{19});
   age1=datenum(doi)-datenum(dob);
   age1=datevec(age1);%[year months days hours minutes seconds]
   fprintf('.');
   
   gender1 = data{14};
   fprintf('.');
   
   race1 = data{4};
   fprintf('.');
   
   id_num1 = data{3};
   fprintf('.');
   
   dir2 = data{1};
   name2 = data{2};
   img_name1 = strcat(dir2, '/', name2, '.jpg');
   img_name2 = strcat(peri_dir, dir2, '/', name2, '_l.ppm');
   img_name3 = strcat(peri_dir, dir2, '/', name2, '_r.ppm');
   img_name4 = strcat(alignedface_dir, dir2, '/', name2, '.ppm');
   fprintf('.');
   
   have_peri1 = zeros(length(img_name2),1);
   for j=1:length(img_name2)
	   have_peri1(j) = (exist(char(img_name2(j)),'file') & exist(char(img_name3(j)),'file')); 
   end
   fprintf('.');
   
   peri_size1 = zeros(length(img_name2),1);
   for j=1:length(img_name2)
       if have_peri1(j) == 1
           im = imread(char(img_name2(j)));
           peri_size1(j) = size(im,1);
       else
           peri_size1(j) = -1;
       end
   end
   fprintf('.');
   
   quality1 = zeros(length(img_name4),1);
   for j=1:length(img_name4)
       if exist(char(img_name4(j)),'file')
           im = rgb2gray(imread(char(img_name4(j))));
           quality1(j) = fmeasure(im, 'FFT2', []);
       else
           quality1(j) = -1;
       end
   end
   fprintf('.');
   
   for j=1:length(img_name2)
       id_num{cur} = id_num1{j};
	   race{cur} = race1{j};
	   gender{cur} = gender1{j};
	   age(cur) = age1(j);
	   img_name{cur} = img_name1{j};
	   have_peri(cur) = have_peri1(j);
	   peri_size(cur) = peri_size1(j);
	   quality(cur) = quality1(j);
	   cur=cur+1;
   end
   fprintf('.\n');
end

fid = fopen('/group/bprl/users/pemille/pinellas1.txt', 'w');
for i=1:length(id_num)
    fprintf(fid, '%s %s %s %d %s %d %d %f\n',id_num{i}, race{i}, gender{i}, age(i), img_name{i}, have_peri(i), peri_size(i), quality(i));
end
fclose(fid);