id = 'icb100001';

xml_file = ['C:\Users\PEMILLE\Documents\Dropbox\Lab Work\code\DeliverableSystem\Experiments\' id '.xml'];
experiment = xml_read(xml_file);
experiment.input.probe='\\skynet\users\pemille\imagelists\2012_03_Dissertation\frgc_quality_all.srt';
experiment.input.gallery='\\skynet\users\pemille\imagelists\2012_03_Dissertation\frgc_quality_all.srt';
experiment.input.datadir='\\skynet\static\Processed_Images\FRGC\FRGC_ALIGNEDFACE\';
%experiment.input.probe='\\skynet\users\pemille\imagelists\2012_03_Dissertation\pinellas30_lefteye_training1_gallery.srt';
%experiment.input.gallery='\\skynet\users\pemille\imagelists\2012_03_Dissertation\pinellas30_lefteye_training1_probe.srt';
%experiment.input.datadir='\\skynet\static\Processed_Images\Pinellas\Pinellas_PERI\';

%%% Scan in image filenames %%%
fid = fopen(experiment.input.probe);
images = textscan(fid, '%s');
fclose(fid);
probes = images{1};
pnum = size(probes,1);

FM3 = ones(pnum,5)*-1; 
for i = 1:pnum
    temp = char(probes(i));
	if exist([experiment.input.datadir temp],'file')
		im = imread([experiment.input.datadir temp]);
		im = rgb2gray(im);
		FM3(i,1) = fmeasure(im, 'FFT2', []);
	end

	
    printing(1, 'preprocessing image', i, pnum);
end
fprintf('\n');


%disp([min(FM3,[],1); max(FM3,[],1); mean(FM3,1); std(FM3,1)]);
FM4 = reshape(FM3(FM3~=-1),[],1);
fprintf('%12.2f',min(FM4,[],1));fprintf('\n');
fprintf('%12.2f',max(FM4,[],1));fprintf('\n');
fprintf('%12.2f',mean(FM4,1));fprintf('\n');
fprintf('%12.2f',std(FM4,1));fprintf('\n');